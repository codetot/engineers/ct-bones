# Changelog

All notable changes to this project will be documented in this file.

## 6.1.2 - 12/11/2021

- Optimize image block
- Composer fix phpcs standards

## 6.1.1 - 11/11/2021

- Update phpcs and composer packages
- Run compose standards fix automatically
- Fix button outline color css style

## 6.1.0 - 06/11/2021

- Set default container in single product sections
- Fix tabs spacing in single product page
- Fix widget product categories spacing
- Fix sanitize hex color meta key
- Enqueue admin editor styles (to render server side block in Block Editor)

## 6.0.10 - 01/11/2021

- Add unit test for library dom.js (WIP) via npm run test:cov

## 6.0.8 - 6.0.9 - 30/10/2021

- Fix #490: Restore slideout menu, modal search form - iOS render bug with React.

## 6.0.7 - 30/10/2021

- Fix break link in post content

## 6.0.6 - 19/10/2021

- Fix slideout menu trigger to open/collapse sub-menu
- Fix slideout menu style

## 6.0.5 - 19/10/2021

- Fix path load for css, js woocommerce.

## 6.0.4 - 18/10/2021

- Fix embed block with aspect ratio, remove legacy class .video-responsive
- Fix spacing header menu item in desktop

## 6.0.3 - 15/10/2021

- Convert Slideout menu to React component
- Convert Modal search form to React component

## 6.0.2 - 15/10/2021

- Fix missing assets CSS woocommerce.
- Fix ci

## 6.0.1 - 15/10/2021

- Fix editor style with link and content layout.
- Fix video responsive style
- Update build workflow CircleCI

## 6.0.0 - 14/10/2021

- Release first Gutenberg compatitible version.

## 5.7.5 - 11/10/2021

- Fix single post template conflict with other post type in child theme
- Replace default metabox page css class with pure metabox
- Remove top footer spacing toggle in admin ui
- Complete remove Metabox requirement from a theme
- Update security npm packages

## 5.7.4 - 05/10/2021

- Update security patchs npm
- Fix wrong constructor in blocks

## 5.7.3

- Fix top header background contract and background
- Load frontend.min.css to ensure all dependencies load correctly
- Change footer copyright text data to Customize settings
- Fix header 3rd level position
- Fix wrong h1 typography font size in 1200.css

## 5.7.2

- Update global typography scale, add `html` font-size attribute responsive screen.
- Disable check `variables.css` for prettier.

## 5.7.1

- Fix default Customizer settings:

```
post_card_style
footer_column
topbar_column
seo_h1_homepage
```

## 5.0.0 - 5.7.0

- Convert from CT-Theme to Customize Settings.
