<?php
/**
 * Footer block
 *
 * @package ct-bones
 * @author codetot
 * @since 0.0.1
 */

do_action( 'codetot_footer' ); ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
