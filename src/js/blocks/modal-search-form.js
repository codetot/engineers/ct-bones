import Modal from 'lib/modal'

export default el => {
	// eslint-disable-next-line no-unused-vars
	const instance = Modal(el, {
		id: 'modal-search-form'
	})
}
